# SEURoboCup2022

东南大学 Robocup Kidsize 2022年校赛代码

遇到问题请先行使用搜索引擎，CSDN，或查阅 [FAQ](https://gitee.com/robocup/SEURoboCup2022/wikis/)

## 1环境配置
Ubuntu及Webots下载:
- Ubuntu20.04
- ROS2
- Webots 2022a [下载链接](https://pan.seu.edu.cn:443/link/977642FBCEAEDBF6471285F3D0E54912)

[Ubuntu20.04+Win10双系统安装](docs/os_install.md)


[ROS2 Webots安装教程](docs/ubuntu.md)


## 2使用方法

### 编译

```Shell
cd ~
git clone https://gitee.com/robocup/SEURoboCup2022
cd SEURoboCup2022
colcon build
```
编译一般在2min以内。

### 初始化

```Shell
sudo pip3 install rosdepc
sudo rosdepc init
```
<!--若报错则参考：[Q4 sudo rosdep init 报错](https://gitee.com/robocup/SEURoboCup2022/wikis/软件环境?sort_id=4351269#q4-sudo-rosdep-init报错)-->
```Shell
rosdepc update
echo "export WEBOTS_HOME=/usr/local/webots" >> ~/.bashrc
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${WEBOTS_HOME}/lib/controller" >> ~/.bashrc
ws=`pwd`
echo "source $ws/install/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

### 创建包

首先须选定一个 **全英文小写字母** 的队名，且其长度不超过 20个字符，也不能为 `test`。以下用 `template` 表示。

！！注意：以下代码无法直接运行，请务必将 `template` 替换为自己的队名！！

输入：
```Shell
cd src # 进入到 SEURoboCup2022/src 文件夹
chmod +x ./createPkg.sh
./createPkg.sh template # 请务必将 template 替换为自己的队名
cd ..
colcon build
```

### 运行

+ 启动仿真相关的节点  
    新建一个终端，输入：
    ```Shell
    ros2 launch start start_launch.py
    # 点球则运行
    # ros2 launch penalty_start start_launch.py
    ```

+ 启动比赛控制器  
    新建一个终端，进入到`SEURoboCup2022`路径，输入：
    ```Shell
    ros2 run gamectrl gamectrl
    # 点球则运行
    # ros2 run penalty_gamectrl gamectrl
    ```

+ 启动我方机器人的控制节点  
    新建一个终端，输入：
    ```Shell
    ros2 launch template player_launch.py
    ```

+ 启动敌方机器人的控制节点  
    新建一个终端，输入：
    ```Shell
    ros2 launch template2 player_launch.py
    ```

+ 若修改过 [player.cpp](src/template/src/player.cpp)，则需要重新编译：
    ```Shell
    colcon build
    ```
