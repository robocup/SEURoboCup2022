# Ubuntu 下安装教程

以下步骤均在 Ubuntu 20.04.1 测试通过，18.04 下可能存在少量问题。

Ubuntu 安装教程请参考 [Ubuntu20.04的安装教程](./os_install.md)

## ROS2 安装

> 主要参考[官方教程](https://index.ros.org/doc/ros2/Installation/Foxy/Linux-Install-Debians/)

以下是详细步骤：

### 设置软件源

![输入图片说明](../assets/1.png)
![输入图片说明](../assets/2.png)

###  首先确保环境支持UTF-8
```shell
locale  # 检查系统是否安装有UTF-8

# 如果没有则进行安装
sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8

locale  # 安装好后再次检查
```
### 设置源
```shell
sudo apt update && sudo apt install curl gnupg2 lsb-release ca-certificates
sudo wget -O /usr/share/keyrings/ros-archive-keyring.gpg https://gitee.com/robocup/SEURoboCup2022/raw/master/docs/ros.key
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg]  https://mirrors.tuna.tsinghua.edu.cn/ros2/ubuntu  focal main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
sudo apt update
```

### ros2与webots的安装
然后进行ros2-foxy的安装，在终端输入：
```Shell
sudo apt install ros-foxy-desktop
```

工具安装：
```Shell
sudo apt update && sudo apt install -y   build-essential   cmake   git   python3-colcon-common-extensions   python3-pip   python3-rosdep   python3-vcstool   wget
pip3 install -U argcomplete
```

然后手动下载webots2022的安装包到Ubuntu中，进入到webots2022的安装目录下，右键进入终端，输入：
```Shell
sudo apt install ./webots_2022a_amd64.deb
sudo apt install -f
echo "source /opt/ros/foxy/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt install ros-$ROS_DISTRO-webots-ros2
```
